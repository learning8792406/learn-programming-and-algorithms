# Commonly Asked Interview Questions (STRIVER SDE SHEET)

&larr; [Back to Coding Problems Home](../../README.md)

## Arrays

| Question                      | Difficulty | Status | Last Solved On    | Topics  | 
| -----------------             |-----       |-----   | -----------       | ------  |
| [Set Zero Matrix](./problems/SetZeroMatrix.md)               | **Medium**  | ✔️ | `4 July, 2022` , `02 February, 2025`     | `Array`, `HashTable`, `Matrix`
| [Pascal's Triangle](./problems/PascalTriangle.md)            | **Easy**       | ✔️ | `4 July, 2022`, `02 February, 2025`    |   `Array`, `Dynamic Programming`
| [Next Permutation](./problems/NextPermutation.md)            | **Medium**     |  ✔️   | `4 July, 2022`, `02 February, 2025` | `Array`, `Two Pointer`
| [Maximum Subarray Sum](./problems/MaxSubArraySum.md)         | **Medium**     |  ✔️   | `4 July, 2022`, `02 February, 2025`      | `Array`, `Divide and Conquer`, `Dynamic Programming`
| [Sort Colors](./problems/SortColors.md)                      | **Medium**    |  ✔️   | 4 July, 2022      | `Array`, `Two Pointers`, `Sorting`
| [Best Time to Buy Stock](./problems/BuySellStocks.md)        | **Easy**       |  ✔️ | 4 July, 2022      | `Array`, `Dynamic Programming`
| [Rotate Image](./problems/RotateImage.md)        | **Medium**    | ✔️   | 5 July, 2022    | `Array`, `Math`, `Matrix`
| [Merge Overlapping subintervals](./problems/MergeIntervals.md)| **Medium**| ✔️ | 07 December, 2022   | `Array`, `String`
| [Merge two sorted Arrays without extra space](./problems/MergeSortedArray.md) | **Easy** | ✔️ | 11 December, 2022   | `Array`, `Two Pointers`, `Sorting` 
|[Find the duplicate in an array of N+1 integers](./problems/FindDuplicate.md) | **Medium** | ✔️ | 21 December, 2022   | `Array` `Two Pointers` `Binary Search` `Bit Manipulation` 
|[Repeat and missing Number](./problems/RepeatAndMissing.md) | **Easy** | ✔️ | 14 January, 2023   | `Bit Manipulation` `Sorting` `Arrays` `Binary Search` 
|Inversion of Array (Pre-req: Merge Sort) |    - | :x: | -   | - 
| [Search a 2d Matrix](./problems/SearchInA2dMatrix.md)               | **Medium**     | ✔️  | 14 January, 2023    | `Array`, `Binary Search`, `Matrix`
|[Pow(X, n)](./problems/pow.md) | **Medium** | ✔️ | 14 January, 2023 | `Math`, `Recursion` 
| [Majority Element (>N/2 times)](./problems/MajorityElement.md) | **Easy** | ✔️ | 14 January, 2023 | `Array` `Hash Table` `Divide and Conquer` `Sorting` `Counting`
| [Majority Element (>N/3 times)](./problems/MajorityElementII.md) | **Medium** | ✔️ | 14 January, 2023 | `Array` `Hash Table` `Sorting` `Counting`
| [Grid Unique Paths](./problems/UniquePaths.md) | **Easy** | ✔️ | 15 January, 2023 | `Math` `Dynamic Programming` `Combinatorics`
| [Reverse Pairs](./problems/ReversePairs.md) | **Hard** | ✔️ | 15 January, 2023 | `Array` `Binary Search` `Divide and Conquer` `Binary` `Indexed Tree` `Segment Tree` `Merge Sort` `Ordered Set`
| [2-Sum Problem](./problems/TwoSum.md)               | **Medium**     | ✔️  | 15 January, 2023      |  `Array` `Two Pointers` `Binary Search`
| [4-Sum Problem](./problems/FourSum.md) | **Medium** | ✔️ | 15 January, 2023 | `Array` `Two Pointers` `Sorting` 
| [Longest Consecutive Sequence](./problems/LongestConsecutiveSequence.md) | **Medium** | ✔️ | 21 January, 2023 | `Array` `Hash Table` `Union Find`
| [Subarray with k sum](./problems/SubarraySumEqualsK.md) | **Medium** | ✔️ | 21 January, 2023 | `Array` `Hash Table` `Prefix Sum`
| [Count number of subarrays with given Xor K](./problems/SubarraysXor.md  ) | **Medium** | ✔️ | 21 January, 2023 | `Array` `Hash Table` `Prefix Sum` `Bit Manipulation` 
| [Longest Substring without repeat](./problems/LongestSubstringWithoutRepeatingCharacters.md) | **Medium** | ✔️ | 21 January, 2023 | `Hash Table` `String` `Sliding Window`
| [three Sum](https://leetcode.com/problems/3sum/) | **Medium** | ✔️ | 19 June, 2023 | `Array` `Two Pointer` `Sorting`
| [Trapping rainwater](https://leetcode.com/problems/trapping-rain-water/) | **Hard** | ✔️ | 19 June, 2023 | `Array` `Two Pointer` `Dynamic Programming`
| [Remove Duplicates from Sorted array](https://leetcode.com/problems/remove-duplicates-from-sorted-array/) | **Easy** | ✔️ | 19 June, 2023 | `Array` `Two Pointer` 
| [Max consecutive ones](https://leetcode.com/problems/max-consecutive-ones/) | **Easy** | ✔️ |  19 June, 2023 | `Array`



## Linked List

| Question                      | Difficulty | Status | Last Solved On    | Topics  | 
| -----------------             |-----       |-----   | -----------       | ------  |
| [Reverse a LinkedList](https://leetcode.com/problems/reverse-linked-list/description/)               | **Easy**  | ✔️  | 04 June, 2023   | `Array`, `Binary Search`, `Matrix`
| [Find the middle of LinkedList](https://leetcode.com/problems/middle-of-the-linked-list/) | **Easy**  | ✔️  | 08 June, 2023 | `Linked List` `Two Pointers` 
| [Merge two sorted Linked List (Use method used in mergeSort)](https://leetcode.com/problems/merge-two-sorted-lists/description/) |**Easy**  | ✔️ | 11 June, 2023 | `Linked List` `Recursion`
| [Remove N-th node from back of LinkedList](https://leetcode.com/problems/remove-nth-node-from-end-of-list/description/) | **Medium** | ✔️ | 11 June, 2023 | `Linked List` `Two Pointers`
| [Add two numbers as LinkedList](https://leetcode.com/problems/add-two-numbers/description/) | **Medium** | ✔️ | 11 June, 2023 | -
| [Delete a given Node when a node is given, (O(1) solution)](https://leetcode.com/problems/delete-node-in-a-linked-list/description/) | **Medium** | ✔️  | 11 June, 2023 | -
| [Intersection of two Linked Lists](https://leetcode.com/problems/intersection-of-two-linked-lists/description/) | **Easy**    | ✔️  | 17 June, 2023 |  
| [Linked List Cycle I](https://leetcode.com/problems/linked-list-cycle/description/) |  **Easy** | ✔️ | 18 June, 2023 | `Floyd's Hare and tortise algorithm`
| [Linked List Cycle II](https://leetcode.com/problems/linked-list-cycle-ii/description/) |  **Medium**  | ✔️ | 18 June, 2023 | - 
| [Reverse node in K Group](https://leetcode.com/problems/reverse-nodes-in-k-group/description/) | **Hard** | ✔️ | 18 June, 2023 || -
| [Palindrome Linked List](https://leetcode.com/problems/palindrome-linked-list/) | **Easy**    | ✔️  | 18 June, 2023 |  `Two Pointer` `Reverse Linked List`
| [Flattening a LinkedList](https://www.codingninjas.com/codestudio/problems/1112655) |  **Easy** | ✔️ | 18 June, 2023  | -
| [Rotate List](https://leetcode.com/problems/rotate-list/description/)               | **Medium**     | ✔️ | 19 June, 2023      | `Two Pointer`
| [Copy List with Random Pointer](https://leetcode.com/problems/copy-list-with-random-pointer/) | **Medium** | ✔️ | 19 June, 2023 | `Hash Table`


## Greedy Algorithm

| Question                      | Difficulty | Status | Last Solved On    | Topics  | 
| -----------------             |-----       |-----   | -----------       | ------  |
| [Maximum Meetings](https://www.codingninjas.com/studio/problems/maximum-meetings_1062658) | **Medium** | ✔️  | 20 June, 2023 | `Sorting`
| [Meeting Rooms II](https://www.interviewbit.com/problems/meeting-rooms/) | **Medium** | ✔️ | 20 June, 2023 | `Sorting`
| [Maximum Profit in Job Scheduling](https://leetcode.com/problems/maximum-profit-in-job-scheduling/) | **Hard** |  ✔️| 20 June, 2023 | -  
| [Fractional Knapsack Problem](https://www.codingninjas.com/studio/problems/fractional-knapsack_975286) | **Easy** | ✔️ | 20 June, 2023 | -
| [Coin Change](https://leetcode.com/problems/coin-change/description/) | **Medium** | ✔️ | 20 June, 2023 | `Dynamic Programming`
| Activity Selection (same as Maximum Meetings) | - | :x: | - | -

## Recurison and Backtracking

| Question                      | Difficulty | Status | Last Solved On    | Topics  | 
| -----------------             |-----       |-----   | -----------       | ------  |
| [Subsets](https://leetcode.com/problems/subsets/)               | **Medium**     | ✔️   | 21 June, 2023      | 
| [Subsets II](https://leetcode.com/problems/subsets-ii/) | **Medium**  | ✔️  |  21 June, 2023   | - 
| [Combination Sum I](https://leetcode.com/problems/combination-sum/) | **Medium**  | ✔️  |  21 June, 2023   | -
| [Combination Sum II](https://leetcode.com/problems/combination-sum-ii/) | **Medium**  | ✔️  |  21 June, 2023   | -
| [Combination Sum III](https://leetcode.com/problems/combination-sum-iii/description/) | **Medium**  | ✔️  |  21 June, 2023   | -
| [Palindrome Partitioning](https://leetcode.com/problems/palindrome-partitioning/) | **Medium**   | ✔️ | 22 June, 2023  | -
| [K-th permutation sequence](https://leetcode.com/problems/permutation-sequence/description/) |  **Hard**  | ✔️ |  22 June, 2023  | -
| [Permutation](https://leetcode.com/problems/permutations/)               | **Medium**      |✔️  | 22 June, 2023      |
| [N queens Problem](https://leetcode.com/problems/n-queens/description/) | **Hard** | ✔️ | 22 June, 2023 | 
| [Sudoku Solver](https://leetcode.com/problems/sudoku-solver/description/) | **Hard** | ✔️ | 23 June, 2023 | 
| M Coloring Problem | - | :x: | - | -
| Rat in a Maze | - | :x: | - | -
| Word Break (Print all ways) | - | :x: | - | -

## Binary Search

| Question                      | Difficulty | Status | Last Solved On    | Topics  | 
| -----------------             |-----       |-----   | -----------       | ------  |
| [Nth Root of integer]()               | - | :x: | - | -
| [Matrix Median]() | - | :x: | - | -
| [Single Element in a Sorted Array](https://leetcode.com/problems/single-element-in-a-sorted-array/description/) | **Medium** | ✔️ | 23 June, 2023 | -
| [Median of Two Sorted Arrays](https://leetcode.com/problems/median-of-two-sorted-arrays/description/) | **Hard** | ✔️| 23 June, 2023 | `Array` `Binary Search` `Divide and Conquer` |
|[K-th element of two sorted arrays]() | - | :x: | - | - |
|[Allocate Minimum Number of Pages]()| - | :x: | - | - |
|[Aggressive Cows]()| - | :x: | - | - |

## Heap
| Question                      | Difficulty | Status | Last Solved On    | Topics  | 
| -----------------             |-----       |-----   | -----------       | ------  |
| [Max heap, Min Heap Implementation]()| -   |:x:     | -                 |         |
| [Kth Largest Element in an Array](https://leetcode.com/problems/kth-largest-element-in-an-array/) | **Medium** | ✔️ | 25 June, 2023 | 
| [Maximum Sum Combination]() | -| :x:  | - | 
| [Find Median from Data Stream](https://leetcode.com/problems/find-median-from-data-stream/) | **Hard** | ✔️| 25 June, 2023 | `Two Pointers` `Design` `Sorting` `Heap (Priority Queue)` `Data Stream`
| [Merge K sorted arrays]() | - | :x: | - | -
| [K most frequent elements]() | **Medium**  | ✔️ | 25 June, 2023 | `Array` `Hash Table` `Divide and Conquer` `Sorting` `Heap (Priority Queue)` `Bucket Sort` `Counting` `Quickselect`

## Stack and Queue

| Question                      | Difficulty | Status | Last Solved On  | Topics  | 
| -----------------             |-----       |-----   | -----------     | ------  |
| [Implement Stack using Arrays]()| -   |:x: | -      |                 |         |
| [Implement Queue using Arrays]()| -   |:x: | -      |                 |         |  
| [Implement Stack using Queues](https://leetcode.com/problems/implement-stack-using-queues/) | **Easy** | ✔️ | 25 June, 2023              |    `Stack` `Design` `Queue`  |
| [Implement Queue using Stacks](https://leetcode.com/problems/implement-queue-using-stacks/)|  **Easy**  |✔️  | 25 June, 2023                |       `Stack` `Design` `Queue`   |
| [Valid Parentheses](https://leetcode.com/problems/valid-parentheses/)| **Easy**   |✔️      | 25 June, 2023                |         |
| [Next Greater Element I](https://leetcode.com/problems/next-greater-element-i/)| **Easy**    | ✔️    | 27 June, 2023                  |         |
| [Sort a Stack]()| -   |:x:     | -                 |         |
| [Next Smaller Element](https://www.codingninjas.com/studio/problems/1112581)| **Medium** | :x:   | -                 |         |
| [LRU Cache](https://leetcode.com/problems/lru-cache/) | **Medium** |  ✔️   | 30 June, 2023                 |         |
| [LFU Cache](https://leetcode.com/problems/lfu-cache/) | **Hard** |  ✔️    | 01 July, 2023                 |         |
| [Largest Rectangle in Histogram](https://leetcode.com/problems/largest-rectangle-in-histogram/) | **Hard** | ✔️  | 01 July, 2023      |         |
| [Sliding Window Maximum](https://leetcode.com/problems/sliding-window-maximum/) | **Hard** |✔️      | 01 July, 2023                   |         |
| [Min Stack](https://leetcode.com/problems/min-stack/) | **Medium** | ✔️     | 01 July, 2023                 |         |
| [Rotting Oranges](https://leetcode.com/problems/rotting-oranges/) | **Medium** | ✔️    | 01 July, 2023                  |         |
| [Online Stock Span](https://leetcode.com/problems/online-stock-span/) | **Medium** | ✔️   | 01 July, 2023                  |         |
| [Find the Celebrity](https://leetcode.com/problems/find-the-celebrity/) | **Medium** | :x:    | -                 |         |



## Strings

| Question                      | Difficulty | Status | Last Solved On  | Topics  | 
| -----------------             |-----       |-----   | -----------     | ------  |
| [Reverse Words in a String](https://leetcode.com/problems/reverse-words-in-a-string/description/)| **Medium**  | ✔️ | 04 July, 2023      |   `Two Pointer`    `String`   |
| [Longest Palindromic Substring](https://leetcode.com/problems/longest-palindromic-substring/description/)| **Medium**  | ✔️ | 04 July, 2023      |  `String` `Dynamic Programming` `Expand from center`  |
| [Longest Common Prefix](https://leetcode.com/problems/longest-common-prefix/description/)| **Easy**  | ✔️ | 01 August, 2023      | `String`  `Trie` |
| [Valid Anagram](https://leetcode.com/problems/valid-anagram/description/)| **Easy**  | ✔️ | 04 July, 2023      |   `Hash Table` `String` `Sorting`   |
| [Count and Say](https://leetcode.com/problems/count-and-say/description/)| **Medium**  | ✔️ | 04 July, 2023      |  `String`  |
| [Compare Version Numbers](https://leetcode.com/problems/compare-version-numbers/description/)| **Medium**  | ✔️ | 01 August, 2023      | `String`  `Two Pointers` |


## Binary Tree
| Question                      | Difficulty | Status | Last Solved On  | Topics  | 
| -----------------             |-----       |-----   | -----------     | ------  |
| [Inorder Traversal](https://leetcode.com/problems/binary-tree-inorder-traversal/description/)| **Easy**  | ✔️ | 05 July, 2023      |   `Two Pointer`    `String`   |
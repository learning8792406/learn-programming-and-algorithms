class Solution:
    # @param A : list of integers
    # @param B : integer
    # @return an integer
    def subsetSum(self, nums, sum, index):
        if index >= len(nums): 
            return 1 if sum == 0 else 0
        else:
            return max(self.subsetSum(nums, sum - nums[index], index+1), self.subsetSum(nums, sum, index+1))

    def solve(self, A, B):
        return self.subsetSum(A, B, 0)



if __name__=='__main__':
    A = [3, 34, 4, 12, 5, 2]
    B = 9
    s = Solution()
    print(s.solve(A, B))
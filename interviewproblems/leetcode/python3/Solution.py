from collections import OrderedDict
import collections
from heapq import heapify, heappop, heappush
import sys
from typing import List, Optional

from interviewproblems.leetcode.python3.ListNode import ListNode

class Node:
    def __init__(self, x: int, next: 'Node' = None, random: 'Node' = None):
        self.val = int(x)
        self.next = next
        self.random = random

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    class TreeNode:
        def __init__(self, val=0, left=None, right=None):
            self.val = val
            self.left = left
            self.right = right


    
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
        slowPtr, fastPtr = head, head

        while fastPtr != None and fastPtr.next != None:
            slowPtr = slowPtr.next
            fastPtr = fastPtr.next.next

        return slowPtr 

    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        resultStart = ListNode(-1)
        finalList = resultStart

        while list1 and list2:
            if list1.val <= list2.val:
                finalList.next = list1
                list1 = list1.next
            else:
                finalList.next = list2
                list2 = list2.next
            finalList = finalList.next
        
        finalList.next = list1 or list2

        return resultStart.next

    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
        currPtr = head
        prevPtr = head

        while currPtr:
            if n > 0:
                n -= 1
            else:
                prevPtr = prevPtr.next
            currPtr = currPtr.next

        prevPtr.next = prevPtr.next.next

        return head

    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        carry = 0
        dummy = ListNode(-1);
        result = dummy

        while l1 and l2:
            sum = l1.val + l2.val + carry
            result.next = ListNode((sum)%10)
            result = result.next
            carry = sum//10

            l1 = l1.next
            l2 = l2.next

        while l1:
            sum = l1.val+carry
            result.next = ListNode(sum%10)
            result = result.next
            carry = sum//10
            l1 = l1.next
            
        
        while l2:
            sum = l2.val+carry
            result.next = ListNode(sum%10)
            result = result.next
            carry = sum//10
            l2 = l2.next

        if carry:
            result.next = ListNode(carry)


        return dummy.next

    def deleteNode(self, node):
        """
        :type node: ListNode
        :rtype: void Do not return anything, modify node in-place instead.
        """
        node.val = node.next.val
        node.next = node.next.next


    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> Optional[ListNode]:
        firstPtr, secondPtr = headA, headB

        while firstPtr != secondPtr:
            firstPtr = firstPtr.next if firstPtr else headB
            secondPtr = secondPtr.next if secondPtr else headA

        return firstPtr
    
    def hasCycle(self, head: Optional[ListNode]) -> bool:
        fastPtr, slowPtr = head, head

        while fastPtr and fastPtr.next:
            fastPtr = fastPtr.next.next
            slowPtr = slowPtr.next

            if slowPtr == fastPtr:
                return True
            
        return False
    
    def detectCycle(self, head: Optional[ListNode]) -> Optional[ListNode]:     
        slowPtr, fastPtr = head, head

        while fastPtr and fastPtr.next:
            slowPtr = slowPtr.next
            fastPtr = fastPtr.next.next

            if slowPtr == fastPtr:
                startPtr = head

                while startPtr != slowPtr:
                    startPtr = startPtr.next
                    slowPtr = slowPtr.next

                return startPtr

        return None

    def reverseSemiList(self, beforePtr, cPtr, afterPtr):
        prevPtr, currPtr, nextPtr = beforePtr, cPtr, cPtr.next

        while currPtr != afterPtr:
            currPtr.next = prevPtr
            prevPtr = currPtr
            currPtr = nextPtr
            if nextPtr:
                nextPtr = nextPtr.next 
        
        cPtr.next = afterPtr
        beforePtr.next = prevPtr

    def reverseKGroup(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        dummy, i = ListNode(0, head), 0
        prevPtr, currPtr, nextPtr = dummy, head, head

        while nextPtr:
            nextPtr = nextPtr.next
            i+=1

            if i == k:
                self.reverseSemiList(prevPtr, currPtr, nextPtr)
                prevPtr = currPtr
                currPtr = nextPtr
                i = 0


        return dummy.next
    
    def isPalindrome(self, head: Optional[ListNode]) -> bool:
        slowPtr, fastPtr = head, head
        
        while fastPtr and fastPtr.next:
            slowPtr = slowPtr.next
            fastPtr = fastPtr.next.next

        prevPtr, currPtr, nextPtr = None, slowPtr, slowPtr.next

        while nextPtr:
            currPtr.next = prevPtr
            prevPtr = currPtr
            currPtr = nextPtr
            nextPtr = nextPtr.next

        currPtr.next = prevPtr
        slowPtr = head
        
        while currPtr and slowPtr:
            if currPtr.val != slowPtr.val:
                return False
            currPtr = currPtr.next
            slowPtr = slowPtr.next 
        
        return True
    

    
    ## Linked List And Arrays
    def rotateRight(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        if not head or not head.next:
            return head
            
        currPtr = head
        n:int = 0
        
        ## length of the linkedlist
        while currPtr:
            currPtr = currPtr.next
            n += 1

        k = k%n 

        fastPtr, slowPtr = head, head

        y:int = 0
        while fastPtr.next:
            if y<k:
                y+=1
            else:
                slowPtr = slowPtr.next
            fastPtr = fastPtr.next

        
        fastPtr.next = head
        head = slowPtr.next
        slowPtr.next = None

        return head
    
    def copyRandomList(self, head: 'Optional[Node]') -> 'Optional[Node]':
        hashMap:dict = {None: None}

        currPtr = head
        while currPtr:
            hashMap[currPtr] = Node(currPtr.val)
            currPtr = currPtr.next

        currPtr = head
        while currPtr:
            currNewPtr = hashMap[currPtr]
            currNewPtr.next = hashMap[currPtr.next] 
            currNewPtr.random = hashMap[currPtr.random] 

            currPtr = currPtr.next

        return hashMap[head]
    
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        result = []
        nums.sort()

        for i in range(len(nums)):
            if i>0 and nums[i-1]==nums[i]: 
                continue 

            j, k = i+1, len(nums)-1
            while j<k:
                total = nums[i] + nums[j] + nums[k]
                if total < 0:
                    j += 1
                elif total > 0:
                    k -= 1
                else:
                    result.append((nums[i], nums[j], nums[k]))
                    j += 1
                    while nums[j-1] == nums[j] and j < k:
                        j += 1

        return result

    def trap(self, height: List[int]) -> int:
        l, r, = 0, len(height) - 1 
        maxL, maxR = height[l], height[r]

        result = 0

        while l <= r:
            if maxL <= maxR:
                result += max(maxL-height[l], 0)
                maxL = max(height[l], maxL)
                l += 1
            else:
                result += max(maxR-height[r], 0)
                maxR = max(height[r], maxR)
                r -= 1
        
        return result
        
    def removeDuplicates(self, nums: List[int]) -> int:
        if len(nums) < 2:
            return len(nums)

        left, right = 0, 1

        while right < len(nums):
            if nums[left] != nums[right]:
                left += 1
                nums[left] = nums[right]
            right+=1
        
        return left+1
    
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        result = 0
        maxSoFar = 0

        for i in nums:
            if i == 1:
                maxSoFar += 1
            else:
                result = max(maxSoFar, result)
                maxSoFar = 0
        
        result = max(maxSoFar, result)
        
        return result
    
    ## Greedy Algorithm
    class Job:
        def __init__(self, id = 0, start=0, end=0, profit=0) -> None:
            self.id = id
            self.start = start 
            self.end = end
            self.profit = profit


    ## this is dynamic programming
    def jobScheduling(self, startTime: List[int], endTime: List[int], profit: List[int]) -> int:
        jobs = [self.Job(i, startTime[i], endTime[i], profit[i]) for i in range(len(profit))]

        jobs.sort(key=lambda x:x.end)

        dHeap = []
        dHeap[0] = 0
        maxProfit = 0
        j = 0

        for job in jobs:
            while dHeap and dHeap[0][0] <= job.start:
                maxProfit = max(maxProfit, heappop(dHeap)[1])
            
            heappush(dHeap, (job.end, maxProfit + job.profit))

        return maxProfit

    def coinChange(self, coins: List[int], amount: int) -> int:
        coins.sort()
        dTable = [amount+1 for i in range(amount+1)]
        dTable[0] = 0

        for coin in coins:
            for i in range(1, len(dTable)):
                if i - coin >= 0:
                    dTable[i] = min(1 + dTable[i-coin], dTable[i])
        
        return dTable[amount] if dTable[amount] != amount+1 else -1
    

    ## Recursion
    def subsetsRecursion(self, nums: List, i: int, subsets: List, currentSet: List):
        if i == len(nums):
            subsets.append(currentSet[:])
            return

        currentSet.append(nums[i])
        self.subsetsRecursion(nums, i+1, subsets, currentSet)
        currentSet.pop()
        self.subsetsRecursion(nums, i+1, subsets, currentSet)

    def subsets(self, nums: List[int]) -> List[List[int]]:
        subsets = []
        self.subsetsRecursion(nums, 0, subsets, [])
        return subsets
    
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        nums.sort()
        subsets = []

        def backtrack(i, subset):
            if i == len(nums):
                subsets.append(subset[:])
                return
            
            subset.append(nums[i])
            backtrack(i+1, subset)
            subset.pop()

            while i+1 < len(nums) and nums[i] == nums[i+1]:
                i += 1
            
            backtrack(i+1, subset)
        
        backtrack(0, [])

        return subsets

    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        candidates.sort()
        combinations = []

        def backtracking(i, target, combination):
            if target < 0 or i >= len(candidates):
                return
            
            if target == 0:
                combinations.append(combination[:])
                return
            
            combination.append(candidates[i])
            backtracking(i, target-candidates[i], combination)
            combination.pop()
            backtracking(i + 1, target, combination) 

        
        backtracking(0, target, [])
        return combinations
    

    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:
        candidates.sort()
        combinations = []

        def backtracking(i, target, combination):
            if target == 0:
                combinations.append(combination[:])
                return
            
            if target < 0 or i >= len(candidates):
                return
            
            combination.append(candidates[i])
            backtracking(i + 1, target-candidates[i], combination)
            combination.pop()
            while i + 1 < len(candidates) and candidates[i] == candidates[i+1]:
                i += 1
                
            backtracking(i + 1, target, combination)
        
        backtracking(0, target, [])
        return combinations
    
    def combinationSum3(self, k: int, n: int) -> List[List[int]]:
        combinations = []

        def backtracking(i:int, target:int, combination:List):
            if target == 0 and len(combination) == k:
                combinations.append(combination[:])
                return
            
            if target < 0 or len(combination) >= k or i >= 10:
                return
            
            for j in range(i, 10):
                combination.append(j)
                backtracking(j+1, target-j, combination)
                combination.pop()

        backtracking(1, n, [])
        return combinations
    
    def partition(self, s: str) -> List[List[str]]:
        partitions = []

        def isPalindrome(l, r) -> bool:
            while l < r:
                if s[l] != s[r]:
                    return False
                l += 1
                r -= 1
            
            return True
        

        def backtrack(i:int, currPartition:List):
            if i >= len(s):
                partitions.append(currPartition[:])
                return
            
            for j in range(i, len(s)):
                if isPalindrome(i, j):
                    currPartition.append(s[i:j+1])
                    backtrack(j+1, currPartition)
                    currPartition.pop()

        backtrack(0, [])

        return partitions
    

    ## Important 
    def getPermutation(self, n: int, k: int) -> str:
        factorial = 1
        numbers = [i for i in range(1, n+1)]
        for i in range (1, n):
            factorial *= i

        result = ""
        k -= 1
        while True:
            index = int(k/factorial)
            result = result + str(numbers[index])
            numbers.remove(numbers[index])
            if len(numbers) == 0:
                break
            
            k = k % factorial
            factorial = factorial/(len(numbers))

        return result
    

    def permute(self, nums: List[int]) -> List[List[int]]:
        permutations = []

        if len(nums) == 1:
            return [nums[:]]
        
        for i in range(len(nums)):
            n = nums.pop(0)
            perms = self.permute(nums)

            for perm in perms:
                perm.append(n)

            permutations.extend(perms)
            nums.append(n)

        return permutations

    def solveNQueens(self, n: int) -> List[List[str]]:
        boards = []
        board = [["." for j in range(n)] for i in range(n)]

        def canPlace(board:List[List[str]], r:int, c:int) -> bool:
            for i in range(n):
                if board[r][i] == 'Q' or board[i][c] == 'Q':
                    return False
            
            for i in range(len(board)):
                for j in range(len(board)):
                    if (i+j == r+c or i-j == r-c) and board[i][j] == 'Q':
                        return False
                        
            return True

        def bactrack(board: List[List[int]], row:int):
            if row == n:
                finalBoard = []
                column = ""

                for r in board:
                    column = ""
                    for c in r:
                        column = column + c
                    finalBoard.append(column)
                     
                boards.append(finalBoard)
                return
            
            for col in range(n):
                if canPlace(board, row, col):
                    board[row][col] = "Q"
                    bactrack(board, row+1)
                    board[row][col] = "."

        bactrack(board, 0)
        return boards
    
    def solveSudoku(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        def canPlace(board: List[List[str]], r: int, c: int, val:str):
            for i in range(len(board)):
                if board[r][i] == val or board[i][c] == val:
                    return False
            
            for i in range(r - r%3, r-r%3 + 3):
                for j in range(c - c%3, c-c%3 + 3):
                    if i != r and j != c and board[i][j] == val:
                        return False
            
            return True
        

        def backtrack(board: List[List[str]], row:int, col:int):

            if row == len(board):
                return
            j = col
            for i in range(row, len(board)):
                while j < len(board):
                    if board[i][j] == '.':
                        for val in range(1, 10):
                            if canPlace(board, i, j, str(val)):
                                board[i][j] = str(val)
                                isPossible = backtrack(board, i, j+1)
                                if isPossible:
                                    return isPossible
                                board[i][j] = "."
                        return False
                    j+=1
                j = 0

            return True

        backtrack(board, 0, 0)


    def singleNonDuplicate(self, nums: List[int]) -> int:
        left, right = 0, len(nums)-1
        while left <= right:
            mid = left + ((right-left)//2)

            if (mid - 1 < 0 or nums[mid] != nums[mid-1]) and (mid + 1 > len(nums)-1 or nums[mid] != nums[mid+1]):
                return nums[mid]
            
            leftSize = mid-1 if nums[mid-1] == nums[mid] else mid

            if leftSize%2:
                r = mid-1
            else:
                l = mid+1

    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        A, B = nums1, nums2
        total = len(A) + len(B)
        half = total//2

        if len(A) > len(B):
            A, B = B, A

        left, right = 0, len(A)-1

        while True:
            midA = left + ((right - left )//2)
            midB = half - midA - 2

            Aleft = A[midA] if midA >= 0 else float("-infinity")
            Aright = A[midA+1] if midA + 1 < len(A) else float("infinity") 
            Bleft = B[midB] if midB >= 0 else float("-infinity")
            Bright = B[midB+1] if midB + 1 < len(B) else float("infinity")

            if Aleft <= Bright and Bleft <= Aright:
                if not total%2:
                    return ((max(Aleft, Bleft) + min(Aright, Bright) )/ 2)
                else:
                    return min(Aright, Bright)
            elif Aleft > Bright:
                right = midA - 1
            else:
                left = midA + 1

    
    ## Heap
    def findKthLargest(self, nums: List[int], k: int) -> int:
        heap = []

        for num in nums:
            heappush(heap, -num)

        i = 0
        result = 0

        while i < k:
            result = heappop(heap)
            i += 1

        return -result
    
    ## Bucket sort is the fastest
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        result:List[int] = []
        counts:dict = {}
        frequency:List[List[int]] = [[] for i in range(len(nums) + 1)]

        for i in nums:
            counts[i] = counts[i] + 1 if i in counts else 1

        for key in counts:
            frequency[counts[key]].append(key)
        
        for i in range(len(nums), -1, -1):
            if k > 0 and frequency[i]:
                for num in frequency[i]:
                    result.append(num)
                    k -= 1

        return result
    

    ## Stack and Queue
    def isValid(self, s: str) -> bool:
        stack = []
        closingP = [')', '}', ']']
        openingP = ['(', '{', '[']
        brackets = dict()
        brackets['('] = ')'
        brackets['{'] = '}'
        brackets['['] = ']'

        for i in s:
            if i in openingP:
                stack.append(i)
            elif i in closingP:
                if stack == [] or i != brackets[stack.pop()]:
                    return False
        
        return True if stack == [] else False

    def nextGreaterElement(self, nums1: List[int], nums2: List[int]) -> List[int]:
        indexDict = {n:i for i, n in enumerate(nums1)}
        result = [-1]*len(nums1)
        
        for j in range(len(nums2)):
            if indexDict.get(nums2[j]) != None:
                for i in range(j+1, len(nums2)):
                    if nums2[i] > nums2[j]:
                        result[indexDict.get(nums2[j])] = nums2[i]
                        break
        return result
    

    def largestRectangleArea(self, heights: List[int]) -> int:
        class Height:
            def __init__(self, index = 0, height= 0):
                self.index = index
                self.height = height

        stack = []
        maxArea = 0

        for currIndex in range(len(heights)):
            start = currIndex
            while stack and stack[-1].height >= heights[currIndex]:
                h = stack.pop()
                maxArea = max(maxArea, h.height*(currIndex - h.index))
                start = h.index
            stack.append(Height(start, heights[currIndex]))
        
        while stack:
            h = stack.pop()
            maxArea = max(maxArea, h.height*(len(heights)-h.index))

        return maxArea

                
    def maxSlidingWindow(self, nums: List[int], k: int) -> List[int]:
        left, right = 0, 0
        deque = []
        result = []

        while right < len(nums):
            while deque and nums[deque[-1]] <= nums[right]:
                deque.pop()
            deque.append(right)

            if deque and left > deque[0]:
                deque.pop(0)

            if right + 1 >= k: 
                result.append(nums[deque[0]])
                left += 1
            right += 1

        return result

    ## 
    def orangesRotting(self, grid: List[List[int]]) -> int:
        time, fresh = 0, 0
        queue = []

        neighbours = [(1,0), (-1,0), (0, 1), (0, -1)]

        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == 2:
                    queue.append((i, j))
                elif grid[i][j] == 1:
                    fresh += 1
        
        while queue and fresh > 0:
            for _ in range(len(queue)):
                r, c = queue.pop(0)
                for nr, nc in neighbours:
                    row = r + nr
                    col = c + nc
                    if row >= 0 and col >= 0 and row < len(grid) and col < len(grid[0]) and grid[row][col] == 1:
                        grid[row][col] = 2
                        queue.append((row, col))
                        fresh -= 1
            time += 1

        return time if fresh == 0 else -1


    ## https://leetcode.com/problems/reverse-words-in-a-string/description/
    def reverseWords(self, s: str) -> str:
        left, right = len(s)-1, len(s)-1
        result = ""

        while left >= 0:
            if s[left] != ' ':
                right = left
                while left >= 0 and s[left] != ' ':
                    left -= 1
                result += s[left+1: right+1] + " "
            else: 
                left -= 1
        
        return result[:-1]
    
    ## https://leetcode.com/problems/valid-anagram/description/
    def isAnagram(self, s: str, t: str) -> bool:
        ## for unicode 
        unicodeTable = [0 for i in range(1112064)]

        for c in s:
            unicodeTable[ord(u'{}'.format(c))] += 1

        for c in t:
            unicodeTable[ord(u'{}'.format(c))] -= 1

        for i in unicodeTable:
            if i != 0:
                return False

        return True
    
    ## https://leetcode.com/problems/longest-palindromic-substring/description/
    def longestPalindrome(self, s: str) -> str:
        result = ""
        length = 0

        for i in range(len(s)):
            ## Odd palindromes
            l, r = i, i
            while l >= 0 and r < len(s) and s[l] == s[r]:
                if r-l+1 > length:
                    result = s[l:r+1]
                    length = r-l+1
                l -= 1
                r += 1

            ## Even Palindrome
            l, r = i, i+1
            while l >= 0 and r < len(s) and s[l] == s[r]:
                if r-l+1 > length:
                    result = s[l:r+1]
                    length = r-l+1
                l -= 1
                r += 1
        
        return result
    
    ## https://leetcode.com/problems/count-and-say/description/
    def countAndSay(self, n: int) -> str:
        if n == 1:
            return "1"
        
        prevResult = self.countAndSay(n-1)
        result = ""
        i = 0
        while i < len(prevResult):
            count = 1
            while i < len(prevResult)-1 and prevResult[i] == prevResult[i+1]:
                i += 1
                count += 1
            
            result += str(count)
            result += str(prevResult[i])

            i += 1 

        return result

    ## https://leetcode.com/problems/compare-version-numbers/
    def compareVersion(self, version1: str, version2: str) -> int:
        version1Values = version1.split('.')
        version2Values = version2.split('.')

        if len(version1Values) > len(version2Values):
            version2Values.extend(['0']*(len(version1Values) - len(version2Values)))  
        else:
            version1Values.extend(['0']*(len(version2Values) - len(version1Values)))  

        for i in range(len(version1Values)):
            if int(version1Values[i]) < int(version2Values[i]):
                return -1
            elif int(version2Values[i]) < int(version1Values[i]):
                return 1

        return 0
    
    ## https://leetcode.com/problems/longest-common-prefix/description/
    def longestCommonPrefix(self, strs: List[str]) -> str:
        currentPrefix = strs[0]
        removeFrom = sys.maxsize
        for i in range(1, len(strs)):
            temp = 0
            a = 0
            for j in range(len(strs[i])):
                if j >= len(currentPrefix) or strs[i][j] != currentPrefix[a]:
                    break                    
                temp += 1
                a+=1 
            removeFrom = min(temp, removeFrom)

        return ''.join(currentPrefix[:removeFrom])

    ## https://leetcode.com/problems/binary-tree-inorder-traversal/description/
    '''
    def inorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        result = []
        def inorderRunner(node: TreeNode):
            if node:
                inorderRunner(node.left)
                result.append(node.val)
                inorderRunner(node.right)
        
        inorderRunner(root)
        return result
    '''

    ## https://leetcode.com/problems/binary-tree-right-side-view/description/
    def inorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        stack = []
        result = []
        currentNode = root

        while currentNode or stack:
            while currentNode:
                stack.append(currentNode)
                currentNode = currentNode.left
            
            lastNode = stack.pop()
            result.append(lastNode.val)
            currentNode = lastNode.right

        return result


    def rightSideView(self, root: Optional[TreeNode]) -> List[int]:
        result = []

        def preorder(currentNode:TreeNode, level:int):
            if not currentNode:
                return
            
            if len(result) == level:
                result.append(currentNode.val)

            preorder(currentNode.right, level+1)
            preorder(currentNode.left, level+1)
        
        preorder(root, 0)
        return result
    

    ## https://leetcode.com/problems/vertical-order-traversal-of-a-binary-tree/description/
    def verticalTraversal(self, root: Optional[TreeNode]) -> List[List[int]]:
        hashMap:dict[dict[list[int]]] = {}
        hashMap[0] = {}
 
        def traversal(currNode:TreeNode, column:int, row:int):
            if currNode:
                traversal(currNode.left, column-1, row+1)
                hashMap[column] = hashMap.get(column, {})
                cordValues = hashMap.get(column, {}).get(row, [])
                cordValues.append(currNode.val)
                hashMap[column][row] = cordValues
                traversal(currNode.right, column+1, row+1)

        traversal(root, 0, 0)

        sortedMap = OrderedDict(sorted(hashMap.items()))

        result = []
        for _, cols in sortedMap.items():
            newList = []
            sortedColsMap = OrderedDict(sorted(cols.items()))
            for _, val in sortedColsMap.items():
                newList.extend(sorted(val))

            result.append(newList)
        
        return result
    

    ## https://leetcode.com/problems/maximum-width-of-binary-tree/description/ 
    def widthOfBinaryTree(self, root: Optional[TreeNode]) -> int:
        queue = []
        maxWidth = 0
        queue.append([root, 1])

        while queue:
            tempQueue = []
            left, right = -1, -1
            while queue:
                currentNode, num = queue.pop(0)
                if currentNode:
                    if left == -1:
                        left = num
                        right = num
                    elif left > 0:
                        right = num  
                    tempQueue.append([currentNode.left, 2*num])
                    tempQueue.append([currentNode.right, 2*num+1])

            maxWidth = max(maxWidth, (right - left + 1) if right > 0 and left > 0 else 0)
            queue.extend(tempQueue)
        
        return maxWidth
    
    ## https://leetcode.com/problems/maximum-depth-of-binary-tree/description/ 
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        def dfsRunner(node:TreeNode, height:int) -> int:
            if node:
                height += 1
                return max(dfsRunner(node.left, height), dfsRunner(node.right, height))
            return height

        return dfsRunner(root, 0)
    

    ## https://leetcode.com/problems/binary-tree-level-order-traversal/description/ 
    def levelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        queue, result = [], []
        if root:
            queue.append(root)

        while queue:
            tempQueue = []
            level = []
            while queue:
                currentNode = queue.pop(0)
                level.append(currentNode.val)
                if currentNode.left:
                    tempQueue.append(currentNode.left)
                if currentNode.right:
                    tempQueue.append(currentNode.right)
            
            result.append(level)
            queue.extend(tempQueue)

        return result
    
    
    ## https://leetcode.com/problems/diameter-of-binary-tree/description/ 
    def diameterOfBinaryTree(self, root: Optional[TreeNode]) -> int:
        global diameter
        diameter = 0
        def postOrderTraversal(node:TreeNode) -> int:
            global diameter
            if node:
                heightLeft = postOrderTraversal(node.left)
                heightRight = postOrderTraversal(node.right)
                diameter = max(diameter, heightLeft + heightRight)
                return 1 + max(heightLeft, heightRight)
            return 0

        postOrderTraversal(root)
        return diameter
    
    ## https://leetcode.com/problems/minimum-size-subarray-sum/description/  
    def minSubArrayLen(self, target: int, nums: List[int]) -> int:
        currSum = 0
        result = sys.maxsize
        left = 0

        for right in range(len(nums)):
            currSum += nums[right]

            while currSum >= target:
                result = min(result, right + 1 - left)
                currSum -= nums[left]
                left += 1
        
        return 0 if result == sys.maxsize else result
    

    ## https://leetcode.com/problems/insert-interval/description/ 
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        result = []
        for i in range(len(intervals)):

            if newInterval[1] < intervals[i][0]:
                result.append(newInterval)
                return result + intervals[i:]
            elif newInterval[0] > intervals[i][1]:
                result.append(intervals[i])
            else:
                newInterval[0] = min(newInterval[0], intervals[i][0])
                newInterval[1] = max(newInterval[1], intervals[i][1])

        result.append(newInterval)
        
        return result
    

    ## https://leetcode.com/problems/minimum-number-of-arrows-to-burst-balloons/description/ 
    def findMinArrowShots(self, points: List[List[int]]) -> int:
        points.sort()
        prevBalloon = points[0]
        result = 1

        for start, end in points[1:]:
            if prevBalloon[1] < start:
                result += 1
                prevBalloon = [start, end]
            else:
                prevBalloon[1] = min(prevBalloon[1], end)


        return result
    

    ## https://leetcode.com/problems/simplify-path/description/ 
    def simplifyPath(self, path: str) -> str:
        stack = []
        currFolder = ""

        for c in path+"/":
            if c == '/':
                if currFolder == "..":
                    if stack: stack.pop()
                elif currFolder != "" and currFolder != ".":
                    stack.append(currFolder)
                currFolder = ""
            else:
                currFolder += c

        return "/" + "/".join(stack)
    

    ## https://leetcode.com/problems/evaluate-reverse-polish-notation/description/ 
    def evalRPN(self, tokens: List[str]) -> int:
        stack = []

        for t in tokens:
            if t in ['*', '/', '+', '-']:
                b = stack.pop()
                a = stack.pop()
                match t:
                    case '+':
                        stack.append(a+b)
                    case '-':
                        stack.append(a-b)
                    case '*':
                        stack.append(int(a*b))
                    case '/':
                        stack.append(int(a/b))
            else:
                stack.append(int(t))

        return stack.pop()
    

    ## https://leetcode.com/problems/partition-list/description/ 
    def partition(self, head: Optional[ListNode], x: int) -> Optional[ListNode]:
        lower = ListNode(-1)
        higher = ListNode(-1)

        currNode, currLow, currHigh = head, lower, higher

        while currNode:
            nextNode = currNode.next
            if currNode.val < x:
                currLow.next = currNode;
                currLow = currLow.next
            else:
                currHigh.next = currNode
                currHigh = currHigh.next
            currNode = nextNode

        currLow.next, currHigh.next = higher.next, None

        return lower.next
    

    ## https://leetcode.com/problems/flatten-binary-tree-to-linked-list/ 
    def flatten(self, root: Optional[TreeNode]) -> None:
        """
        Do not return anything, modify root in-place instead.
        """

        def flattenRunner(currNode: TreeNode) -> TreeNode:
            if not currNode:
                return None
            
            leftTail = flattenRunner(currNode.left)
            rightTail = flattenRunner(currNode.right)

            if currNode.left:
                leftTail.right = currNode.right
                currNode.right = currNode.left
                currNode.left = None
            
            return rightTail or leftTail or currNode
        
        flattenRunner(root)


    def sumNumbers(self, root: Optional[TreeNode]) -> int:
        global result
        result = 0
        def traversal(currNode: TreeNode, currSum: str):
            global result
            if not currNode.left and not currNode.right:
                result += int(currSum + str(currNode.val))
                return

            traversal(currNode.left, currSum+str(currNode.val))
            traversal(currNode.right, currSum+str(currNode.val))
                
        traversal(root, "")
        return result
    

    ## https://leetcode.com/problems/sum-root-to-leaf-numbers/description/ 
    def sumNumbers(self, root: Optional[TreeNode]) -> int:
        global result
        result = 0
        def traversal(currNode: TreeNode, currSum: str):
            global result
            if not currNode:
                return 
            if not currNode.left and not currNode.right:
                result += int(currSum + str(currNode.val))
                return

            traversal(currNode.left, currSum+str(currNode.val))
            traversal(currNode.right, currSum+str(currNode.val))
                
        traversal(root, "")
        return result
        

    ## https://leetcode.com/problems/count-complete-tree-nodes/description/ 
    def countNodes(self, root: Optional[TreeNode]) -> int:
        global result
        result = 0
        def traversal(currNode:TreeNode):
            global result
            if currNode:
                traversal(currNode.left)
                result += 1
                traversal(currNode.right)

        traversal(root)
        return result
    

    ## https://leetcode.com/problems/average-of-levels-in-binary-tree/description/
    def averageOfLevels(self, root: Optional[TreeNode]) -> List[float]:
        global result
        result = []
        def traversal(currNode: TreeNode, level: int): 
            global result
            if not currNode:
                return
            
            if len(result) == level:
                result.append([0, 0])

            result[level][0] += currNode.val
            result[level][1] += 1

            traversal(currNode.left, level+1)
            traversal(currNode.right, level+1)
        
        traversal(root, 0)
        return list(map(lambda x: x[0]/x[1], result))


    ## https://leetcode.com/problems/minimum-absolute-difference-in-bst/description/ 
    def getMinimumDifference(self, root: Optional[TreeNode]) -> int:
        prevNode:TreeNode = None 
        minDiff = sys.maxsize

        def traversal(currNode: TreeNode):
            if currNode:
                nonlocal prevNode, minDiff
                traversal(currNode.left)

                if prevNode:
                    minDiff = min(minDiff, currNode.val - prevNode.val)

                prevNode = currNode
                traversal(currNode.right)
        
        traversal(root)
        return minDiff
    

    def minDiffInBST(self, root: Optional[TreeNode]) -> int:
        prevNode:TreeNode = None
        minDiff = sys.maxsize

        def traversal(currNode: TreeNode):
            if currNode:
                nonlocal prevNode, minDiff
                traversal(currNode.left)

                if prevNode:
                    minDiff = min(minDiff, currNode.val - prevNode.val)

                prevNode = currNode
                traversal(currNode.right)

        traversal(root)
        return minDiff
    

    ## https://leetcode.com/problems/surrounded-regions/description/ 
    def solve(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        unsurroundedNodes = []
        directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        m, n = len(board), len(board[0])

        for i in range(m):
            for j in range(n):
                if i == 0 or i == m - 1 or j == 0 or j == n - 1:
                    if board[i][j] == 'O':
                        unsurroundedNodes.append((i, j))

        def dfsTraversal(node:tuple[int]):
            nonlocal m, n, directions;
            i, j = node[0], node[1]
            
            if node:
                if board[node[0]][node[1]] == 'O':
                    board[node[0]][node[1]] = 'V'
                for c1, c2  in directions:
                    ni, nj = i + c1, j + c2
                    if ni >= 0 and ni < m and nj >= 0 and nj < n and board[ni][nj] == 'O':
                        dfsTraversal((ni, nj))

        for i, j in unsurroundedNodes:
            if board[i][j] == 'O':
                dfsTraversal((i, j)) 

        
        for i in range(m):
            for j in range(n):
                if board[i][j] == 'O':
                    board[i][j] = 'X'
                elif board[i][j] == 'V':
                    board[i][j] = 'O'
    

    ## https://leetcode.com/problems/evaluate-division/description/ 
    def calcEquation(self, equations: List[List[str]], values: List[float], queries: List[List[str]]) -> List[float]:
        adjList = collections.defaultdict(list)

        for i, eq in enumerate(equations):
            a, b = eq
            adjList[a].append((b, values[i]))
            adjList[b].append((a, 1/values[i]))

        
        def bfsTraversal(source:str, destination:str) -> float:
            nonlocal adjList
            if source not in adjList or destination not in adjList:
                return -1.0
            
            queue = [source]
            visited = set()
            parent = {}
            currentSum = 1;

            while queue:
                currNode = queue.pop()
                visited.add(currNode)
                for n, value in adjList[currNode]:
                    if n not in visited:
                        queue.append(n)
                        parent[n] = currNode
            
            if destination in visited:
                current = destination

                while parent.get(current):
                    p = parent[current]
                    value = 0
                    for i, j in adjList[p]:
                        if i == current:
                            value = j
                            break;

                    currentSum *= value 
                    current = p

                return currentSum
            
            return -1.0 

        return [bfsTraversal(s, d) for s, d in queries]
    

    '''
    def findOrder(self, numCourses: int, prerequisites: List[List[int]]) -> List[int]:
        adj = collections.defaultdict(list)

        for course, prerequisite in prerequisites:
            adj[course].append(prerequisite)
        
        completed = []
        visited, cycleDetection = set(), set() 
        

        def dfsTraversal(currCourse:int) -> int:
            nonlocal visited, cycleDetection, completed

            if currCourse in cycleDetection:
                return False
            
            if currCourse in visited:
                return True

            cycleDetection.add(currCourse)
            for preReq in adj[currCourse]:
                if dfsTraversal(preReq) == False:
                    return False
            cycleDetection.remove(currCourse)
            visited.add(currCourse)
            completed.append(currCourse)
                

        
        if len(completed) != numCourses:
            return []
        
        return order
    '''


    def findOrder(self, numCourses: int, prerequisites: List[List[int]]) -> List[int]:
        adj = collections.defaultdict(list)
        outdegree = [0 for i in range(numCourses)]

        for course, prerequisite in prerequisites:
            adj[prerequisite].append(course)
            outdegree[course] += 1
        
        visited = set()
        completed = []
        queue = []

        for i in range(numCourses):
            if outdegree[i] == 0:
                queue.append(i)

        while queue:
            currCourse = queue.pop(0)
            if currCourse not in visited:
                visited.add(currCourse)
                completed.add(currCourse)

            for dependantCourse in adj[currCourse]:
                if dependantCourse not in visited:
                    outdegree[dependantCourse] -= 1
                    if outdegree[dependantCourse] == 0:
                        queue.append(dependantCourse)


        if len(visited) != numCourses:
            return []
        
        return completed            

    ## https://leetcode.com/problems/snakes-and-ladders/description/
    def snakesAndLadders(self, board: List[List[int]]) -> int:
        n = len(board)
        board.reverse()

        def getCordinates(currPos):
            row = (currPos-1)//n
            if row % 2 == 0:
                col = (currPos - 1) % n
            else:   
                col = n - ((currPos - 1) % n) - 1

            return [row, col]

        queue = []            
        visited = set()
        queue.add([1, 0])

        while queue:
            currPos, moves = queue.pop(0)
            for i in range(1, 7):
                nextPos = currPos + i
                r, c = getCordinates(nextPos)

                if board[r][c] != -1:
                    nextPos = board[r][c]

                if nextPos == n * n:
                    return moves + 1

                if nextPos not in visited:
                    visited.add(nextPos)
                    queue.append([nextPos, moves+1])

        return -1
    

    ## https://leetcode.com/problems/minimum-genetic-mutation/description/ 
    def minMutation(self, startGene: str, endGene: str, bank: List[str]) -> int:
        queue = []
        mutated = set()
        mutated.add(startGene)
        queue.append([startGene, 0])

        while queue:
            currGene, mutationCount = queue.pop(0)

            for i, g in enumerate(currGene):
                for c in 'ACGT':
                    if c != g:
                        mutatedGene = currGene[:i] + c + (currGene[i+1:] if i < len(currGene)-1 else '')

                        if mutatedGene not in bank:
                            continue
                        
                        if mutatedGene == endGene:
                            return mutationCount + 1

                        if mutatedGene not in mutated:
                            mutated.add(mutatedGene)
                            queue.append([mutatedGene, mutationCount + 1])

        return -1
    



    ##  https://leetcode.com/problems/construct-quad-tree/description/ 
    def construct(self, grid: List[List[int]]) -> 'Node':

        class Node:
            def __init__(self, val, isLeaf, topLeft, topRight, bottomLeft, bottomRight):
                self.val = val
                self.isLeaf = isLeaf
                self.topLeft = topLeft
                self.topRight = topRight
                self.bottomLeft = bottomLeft
                self.bottomRight = bottomRight

        def dfsTraversal(n:int, rStart:int, cStart:int):
            nonlocal grid

            allSame = True
            for i in range(n):
                for j in range(n):
                    if grid[rStart+i][cStart + j] != grid[rStart+n-1][cStart+n-1]:
                        allSame = False
                        break
            
            if allSame:
                return Node(grid[rStart][cStart], True)
            
            nonLeaf = Node(1, False)

            nonLeaf.topLeft = dfsTraversal(n//2, rStart, cStart)
            nonLeaf.topRight = dfsTraversal(n//2, rStart, cStart+n//2)
            nonLeaf.bottomLeft = dfsTraversal(n//2, rStart+n//2, cStart)
            nonLeaf.bottomRight = dfsTraversal(n//2, rStart+n//2, cStart+n//2)

            return nonLeaf
        

        return dfsTraversal(len(grid), 0, 0)

    ## https://leetcode.com/problems/h-index/description/
    def hIndex(self, citations: List[int]) -> int:
        citations.sort(reverse=True)
        currentH = 0
        for i, citation in enumerate(citations):
            if citation >= i+1:
                currentH += 1

        return currentH

    ## https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/
    def findMin(self, nums: List[int]) -> int:

        def specialBinary(left:int, right:int) -> int:
            nonlocal nums
            if(left <= right) :
                mid = left + ((right - left)//2)

                if right == left or nums[mid-1] > nums[mid]:
                    return nums[mid]
                
                if nums[mid] > nums[right]:
                    return specialBinary(mid + 1, right)
                else:
                    return specialBinary(left, mid - 1)

        return specialBinary(0, len(nums)-1)


    ## https://leetcode.com/problems/find-k-pairs-with-smallest-sums/description/ 
    def kSmallestPairs(self, nums1: List[int], nums2: List[int], k: int) -> List[List[int]]:
        from heapq import heappush, heappop

        m = len(nums1)
        n = len(nums2)

        result = []
        visited = set()

        minHeap = [[(nums1[0]+nums2[0]), 0, 0]]
        visited.add((0, 0))

        while k > 0 and minHeap:
            val, i, j = heappop(minHeap)
            result.append([nums1[i], nums2[j]])

            if i + 1 < m and (i+1, j) not in visited:
                heappush(minHeap, [(nums1[i+1] + nums2[j]), i+1, j])
                visited.add((i+1, j))
            
            if j + 1 < n and (i, j+1) not in visited:
                heappush(minHeap, [(nums1[i] + nums2[j+1]), i, j+1])
                visited.add((i, j+1))

            k -= 1

        return result
    

    ## https://leetcode.com/problems/triangle/
    def minimumTotal(self, triangle: List[List[int]]) -> int:
        dpTable = [[sys.maxsize for i in range(len(triangle[-1]))] for j in range(len(triangle))]
        dpTable[0][0] = triangle[0][0]

        for i in range(1, len(triangle)):
            for j in range(len(triangle[i-1])):
                dpTable[i][j] = min(dpTable[i][j], dpTable[i-1][j] + triangle[i][j])
                dpTable[i][j+1] = min(dpTable[i][j+1], dpTable[i-1][j] + triangle[i][j+1])

        return min(dpTable[-1])
                

    ## https://leetcode.com/problems/interleaving-string/description/ 
    def isInterleave(self, s1: str, s2: str, s3: str) -> bool:
        if len(s1) + len(s2) != len(s3):
            return False

        dpTable = [[False for j in range(len(s2) + 1)] for i in range(len(s1) + 1)]
        dpTable[-1][-1] = True

        for i in range(len(s1), -1, -1):
            for j in range(len(s2), -1, -1):
                if i < len(s1) and s1[i] == s3[i + j] and dpTable[i+1][j]:
                    dpTable[i][j] = True
                if j < len(s2) and s2[j] == s3[i + j] and dpTable[i][j+1]:
                    dpTable[i][j] = True
        
        return dpTable[0][0]
                    

    ## https://leetcode.com/problems/maximal-square/ 
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        m = len(matrix)
        n = len(matrix[0])
        cache = [[0 for j in range(n)] for j in range(m)]

        for i in range(m-1, -1, -1):
            for j in range(n-1, -1, -1):
                if i+1 < m and j+1 < n and matrix[i][j] == '1':
                    cache[i][j] = int(matrix[i][j]) + min(cache[i+1][j], cache[i][j+1], cache[i+1][j+1])
                else:
                    cache[i][j] = int(matrix[i][j])

        return (max(max(x) for x in cache))**2
    

    ## https://leetcode.com/problems/single-number-ii/ 
    def singleNumber(self, nums: List[int]) -> int:
        ones = 0
        twos = 0

        for i in nums:
            ones = (ones^i) & ~twos
            twos = (twos^i) & ~ones

        return ones


    ## https://leetcode.com/problems/product-of-array-except-self/description/
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        result = [0 for _ in range(len(nums))]

        pre, post = 1, 1

        for i, n in enumerate(nums):
            result[i] = pre
            pre *= n

        for i, n in reversed(list(enumerate(nums))):
            result[i] *= post
            post *= n


        return result
    
    ## https://leetcode.com/problems/bitwise-and-of-numbers-range 
    def rangeBitwiseAnd(self, left: int, right: int) -> int:
        answer = 0

        for bit in range(30, -1, -1):
            if (left & 1 << bit) != (right & 1 << bit):
                break
            else:
                answer |= (left & 1 << bit)

        return answer
    

    ## 
    def wordPattern(self, pattern: str, s: str) -> bool:
        s = s.split(" ")
        if len(pattern) != len(s):
            return False
         
        charDictionary = {}
        wordDictionary = {}

        for i, p in enumerate(pattern):
            if charDictionary.get(p) and s[i] != charDictionary[p]:
                return False 
            elif wordDictionary.get(s[i]) and wordDictionary[s[i]] != p:
                return False
            else:
                charDictionary[p] = s[i]
                wordDictionary[s[i]] = p

        return True
        

    ## https://leetcode.com/problems/candy/description/ 
    def candy(self, ratings: List[int]) -> int:
        candies = [1 for i in range(len(ratings))]

        for i in range(len(ratings)):
            if i > 0 and ratings[i] > ratings[i-1]:
                candies[i] = candies[i-1] + 1

        
        for i in range(len(ratings)-1, -1, -1):
            if i < len(ratings)-1 and ratings[i] > ratings[i+1]:
                candies[i] = max(candies[i], candies[i+1] + 1)
        
        return sum(candies)

        
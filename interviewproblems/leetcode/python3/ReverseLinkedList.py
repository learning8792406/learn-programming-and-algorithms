from typing import Optional
from leetcode.python3.ListNode import ListNode

class Solution:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if head == None:
            return head

        prevNode, currNode, nextNode = None, head, head.next

        while currNode != None:
            currNode.next = prevNode
            prevNode = currNode
            currNode = nextNode
            if nextNode != None:
                nextNode = nextNode.next
        
        return prevNode

    def reverseBetween(self, head: Optional[ListNode], left: int, right: int) -> Optional[ListNode]:
        counter = 1
        prevNode, currNode, nextNode = head, head, head.next

        while prevNode.next != None:
            if counter == left-1:
                prevNode, currNode, nextNode = prevNode, prevNode.next, prevNode.next.next if prevNode.next != None else None 
                while counter != right:
                    currNode.next = prevNode
                    prevNode = currNode
                    currNode = nextNode
                    if nextNode != None:
                        nextNode = nextNode.next
                    counter += 1
                continue
            prevNode =  prevNode.next
            counter += 1

        return prevNode




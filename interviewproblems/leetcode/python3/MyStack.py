class MyStack:
    def __init__(self):
        self.queueOne = []
        self.queueTwo = [] 

    def push(self, x: int) -> None:
        if self.queueOne == []:
            self.queueOne.append(x)
            while self.queueTwo != []:
                self.queueOne.append(self.queueTwo.pop(0))
        else:
            self.queueTwo.append(x)
            while self.queueOne != []:
                self.queueTwo.append(self.queueOne.pop(0))

    def pop(self) -> int:
        if self.queueOne == []:
            return self.queueTwo.pop(0)
        else:
            return self.queueOne.pop(0)
        

    def top(self) -> int:
        if self.queueOne == []:
            return self.queueTwo[0]
        else:
            return self.queueOne[0]
        

    def empty(self) -> bool:
        return self.queueOne == [] and self.queueTwo == []
        


# Your MyStack object will be instantiated and called as such:
# obj = MyStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.empty()
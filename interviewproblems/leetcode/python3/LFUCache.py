import collections


class DLinkedList:
    def __init__(self, key=-1, value=-1, prev=None, next=None):
        self.key = key
        self.value = value
        self.prev = prev
        self.next = next

class LRUList:
    def __init__(self, ):
        self.counter = 0
        self.left = DLinkedList()
        self.right = DLinkedList()
        self.hashMap = dict()

        self.left.next = self.right
        self.right.prev = self.left

    def length(self):
        return len(self.hashMap)
    
    ## from left of the list
    def pop(self, node:DLinkedList)->DLinkedList:
        if node.key in self.hashMap:
            node = self.hashMap[node.key]
            node.prev.next = node.next
            node.next.prev = node.prev
            node.next = None
            node.prev = None

            self.hashMap.pop(node.key)

            self.counter -= 1

    ## from right of the list
    def push(self, node:DLinkedList):
        node.next = self.right
        node.prev = self.right.prev 
        self.right.prev.next = node
        self.right.prev = node

        self.hashMap[node.key] = node
        self.counter += 1

    def updateLRU(self, node:DLinkedList):
        self.pop(node)
        self.push(node)

    def removeLRU(self):
        if self.left.next != self.right:
            self.pop(self.left.next)


    def get(self, key: int) -> int:
        if key in self.hashMap:
            self.updateLRU(self.hashMap[key])
            return self.hashMap[key].value
        
        return -1
        
    def put(self, key: int, value: int) -> None:
        if key in self.hashMap:
            self.pop(self.hashMap[key])
        else:
            self.push(DLinkedList(key, value))
        

class LFUCache:

    def __init__(self, capacity: int):
        self.capacity = capacity
        self.lfuCount = 0
        # key -> value
        self.valueMap = {}
        # key -> count
        self.countMap = collections.defaultdict(int)
        # key -> LRUList
        self.listMap = collections.defaultdict(LRUList)

    def counter(self, key):
        count = self.countMap[key]
        self.countMap[key] += 1
        self.listMap[count].pop(DLinkedList(key, self.valueMap[key]))
        self.listMap[count+1].push(DLinkedList(key, self.valueMap[key]))

        if count == self.lfuCount and self.listMap[count].length() == 0:
            self.lfuCount += 1    

    def get(self, key: int) -> int:
        if key not in self.valueMap:
            return -1
        self.counter(key)
        return self.valueMap[key]; 
        

    def put(self, key: int, value: int) -> None:
        if self.capacity == 0:
            return
        
        if key not in self.valueMap and len(self.valueMap) == self.capacity:
            removedNode = self.listMap[self.lfuCount].removeLRU()
            self.valueMap.pop(removedNode.key)
            self.countMap.pop(removedNode.key)

        
        self.valueMap[key] = value
        self.counter(key)
        self.lfuCount = min(self.lfuCount, self.countMap[key])
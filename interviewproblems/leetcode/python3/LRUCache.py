class DLinkedList:
    def __init__(self, key=-1, value=-1, prev=None, next=None):
        self.key = key
        self.value = value
        self.prev = prev
        self.next = next

class LRUCache:

    def __init__(self, capacity: int):
        self.counter = 0
        self.capacity = capacity
        self.left = DLinkedList()
        self.right = DLinkedList()
        self.hashMap = dict()

        self.left.next = self.right
        self.right.prev = self.left

    def length(self):
        return len(self.hashMap)

    ## from left of the list
    def pop(self, node:DLinkedList)->DLinkedList:
        print(self.hashMap)
        print(node.prev)
        node.prev.next = node.next
        node.next.prev = node.prev
        node.next = None
        node.prev = None

        self.hashMap.pop(node.key)

    ## from right of the list
    def push(self, node:DLinkedList):
        if node.key in self.hashMap:
            self.pop(node)

        node.next = self.right
        node.prev = self.right.prev 
        self.right.prev.next = node
        self.right.prev = node

        self.hashMap[node.key] = node

    def updateLRU(self, node:DLinkedList):
        self.pop(node)
        self.push(node)

    def removeLRU(self):
        if self.left.next != self.right:
            return self.pop(self.left.next)


    def get(self, key: int) -> int:
        if key in self.hashMap:
            self.updateLRU(self.hashMap[key])
            return self.hashMap[key].value
        
        return -1
        
    def put(self, key: int, value: int) -> None:
        if self.counter == self.capacity:
            self.removeLRU()
            self.push(DLinkedList(key, value))
        else:
            self.push(DLinkedList(key, value))
            self.counter += 1
class MyQueue:
    def __init__(self):
        self.stackTwo = []
        self.stackOne = []
    
    def push(self, x: int) -> None:
        self.stackOne.append(x)   

    def pop(self) -> int:
        while self.stackOne != []:
            self.stackTwo.append(self.stackOne.pop())
        frontVal = self.stackTwo.pop()
        while self.stackTwo != []:
            self.stackOne.append(self.stackTwo.pop())

        return frontVal

    def peek(self) -> int:
        return self.stackOne[0]

    def empty(self) -> bool:
        return self.stackOne == [] and self.stackTwo == []
        

# Your MyQueue object will be instantiated and called as such:
# obj = MyQueue()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.peek()
# param_4 = obj.empty()
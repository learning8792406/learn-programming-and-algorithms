from typing import Optional

from leetcode.python3.ListNode import ListNode


class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        newList: ListNode = ListNode(-1, None)
        l1Ptr, l2Ptr, newListPtr = list1, list2, newList
        while l1Ptr.next != None or l2Ptr.next != None:         # type: ignore
            if l2Ptr == None or l1Ptr.val <= l2Ptr.val:         # type: ignore
                newListPtr.next = l1Ptr
                l1Ptr = l1Ptr.next                              # type: ignore
            else:
                newListPtr.next = l2Ptr
                l2Ptr = l2Ptr.next                              # type: ignore
            newListPtr = newListPtr.next                        # type: ignore
    
        return newList.next

from typing import Optional

from leetcode.python3.ListNode import ListNode


class Solution:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
        fastPtr:ListNode|None = head 
        slowPtr:ListNode|None = head

        while fastPtr != None and fastPtr.next != None:
            slowPtr = slowPtr.next  # type: ignore
            fastPtr = fastPtr.next.next
        
        return slowPtr

    
from os import *
from sys import *
from collections import *
from math import *

class Meetings:
    def __init__(self, id = 0, start = 0, end = 0) -> None:
        self.start = start
        self.end = end
        self.id = id

def maximumMeetings(start, end):
    meetings = [Meetings(i+1, start[i], end[i]) for i in range(len(start))]

    meetings.sort(key=lambda x:x.end)
    result = []
    currentFinishTime = meetings[0].end 

    result.append(meetings[0].id)
    for i in range(1, len(meetings)):
        if meetings[i].start > currentFinishTime:
            currentFinishTime = meetings[i].end
            result.append(meetings[i].id)
    
    return result


def maximumValue(items, n, w):
    items.sort(key=lambda x: x[1]/x[0], reverse=True)
    
    result = 0
    currWeight = 0
    for item in items:
        if currWeight + item[0] <= w:
            currWeight += item[0]
            result += item[1]
        else:
            result += (item[1] * ((w - currWeight) / item[0]*1.0))
            break

    return result

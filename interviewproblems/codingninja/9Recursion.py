from os import *
from sys import *
from collections import *
from math import *

from typing import List

def subsetSumRecursion(nums, i, sum, subset):
    if i == len(nums):
        subset.add(sum)
        return
    
    subsetSumRecursion(nums, i+1, sum+nums[i], subset)
    subsetSumRecursion(nums, i+1, sum, subset)

def subsetSum(num: List[int]) -> List[int]:
    subset = set()
    subsetSumRecursion(num, 0, 0, subset)
    
    return sorted(list(subset))

if __name__=='__main__':
    num = [1, 2]
    print(subsetSum(num))

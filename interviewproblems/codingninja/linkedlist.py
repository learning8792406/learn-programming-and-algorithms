# Coding Ninja's LinkedList
class Node:
    def __init__(self, val=0, next=None, child=None):
        self.data = val
        self.next = next
        self.child = child

        
def mergeTwoListsBottom(list1: Node, list2: Node) -> Node:
    resultStart = Node(-1)
    list2Start = list2
    finalList = resultStart

    while list1 and list2:
        if list1.data <= list2.data:
            finalList.child = list1
            list1 = list1.child 
        else:
            finalList.child = list2
            list2 = list2.child 
        finalList = finalList.child
    
    finalList.child = list1 or list2

    resultStart.child.next = list2Start.next 

    return resultStart.child

def flattenLinkedList(head: Node) -> Node:
    finalList = head

    while finalList.next:
        finalList = mergeTwoListsBottom(finalList, finalList.next)

    return finalList
from os import *
from sys import *
from collections import *
from math import *

# Following is the Binary Tree node structure:
class BinaryTreeNode :
    def __init__(self, data) :
        self.data = data
        self.left = None
        self.right = None

def getTreeTraversal(root):
    stack = []
    preorder = []
    inorder = []
    postorder = []
    result = []

    stack.append((root, 1))

    while stack:
        currNode = stack.pop()

        if currNode[1] == 1:
            preorder.append(currNode[0])
            currNode[1] += 1
            stack.append(currNode)

            if currNode[0].left:
                stack.append((currNode[0].left, 1))
        elif currNode[1] == 2:
            inorder.append(currNode[0])
            currNode[1] += 1
            stack.append(currNode)

            if currNode[0].right:
                stack.append((currNode[0].right, 1))
        else:
            postorder.append(currNode[0])

    result.append(preorder)
    result.append(inorder)
    result.append(postorder)

    return result

if __name__ == '__main__':
    pass